defmodule Rumbl.Multimedia.Permalink do
  @behaviour Ecto.Type

  @impl Ecto.Type
  def type, do: :id

  @impl Ecto.Type
  def cast(binary) when is_binary(binary) do
    case Integer.parse(binary) do
      {int, _} when int > 0 -> {:ok, int}
      _ -> :error
    end
  end

  @impl Ecto.Type
  def cast(integer) when is_integer(integer) do
    {:ok, integer}
  end

  @impl Ecto.Type
  def cast(_) do
    :error
  end

  @impl Ecto.Type
  def dump(integer) when is_integer(integer) do
    {:ok, integer}
  end

  @impl Ecto.Type
  def load(integer) when is_integer(integer) do
    {:ok, integer}
  end

  @impl Ecto.Type
  def embed_as(_format), do: :self

  @impl Ecto.Type
  def equal?(term1, term2), do: term1 == term2
end
