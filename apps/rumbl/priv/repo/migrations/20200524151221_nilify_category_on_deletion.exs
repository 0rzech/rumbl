defmodule Rumbl.Repo.Migrations.NillifyCategoryOnDeletion do
  use Ecto.Migration

  def change do
    alter table(:videos) do
      modify :category_id,
        references(:categories, on_delete: :nilify_all),
        from: references(:categories)
    end
  end
end
