defmodule InfoSys.Test.HTTPClient do
  @wolfram_xml File.read!("test/fixtures/wolfram.xml")

  def request(%Finch.Request{} = req, _name, _opts \\ []) do
    cond do
      String.contains?(req.query, "1+%2B+1") ->
        {:ok, %Finch.Response{status: 200, body: @wolfram_xml, headers: []}}

      true ->
        {:ok, %Finch.Response{status: 200, body: "<queryresult></queryresult>", headers: []}}
    end
  end
end
