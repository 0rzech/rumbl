defmodule InfoSysTest do
  use ExUnit.Case

  alias InfoSysTest.TestBackend
  alias InfoSys.Result

  test "compute/2 with backend results" do
    assert [%Result{backend: TestBackend, text: "result"}] =
      InfoSys.compute("result", backends: [TestBackend])
  end

  test "compute/2 with no backend results" do
    assert [] = InfoSys.compute("none", backends: [TestBackend])
  end

  test "compute/2 with timout returns no results" do
    results = InfoSys.compute("timeout", backends: [TestBackend], timeout: 10)
    assert results == []
  end

  @tag :capture_log
  test "compute/2 discards backend errors" do
    assert InfoSys.compute("boom", backends: [TestBackend]) == []
  end

  defmodule TestBackend do
    @behaviour InfoSys.Backend

    @impl InfoSys.Backend
    def name(), do: "Wolfram"

    @impl InfoSys.Backend
    def compute("result", _opts) do
      [%Result{backend: __MODULE__, text: "result"}]
    end

    @impl InfoSys.Backend
    def compute("none", _opts), do: []

    @impl InfoSys.Backend
    def compute("timeout", _opts), do: Process.sleep(:infinity)

    @impl InfoSys.Backend
    def compute("boom", _opts), do: raise "boom!"
  end
end
