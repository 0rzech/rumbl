defmodule RumblWeb.VideoControllerTest do
  use RumblWeb.ConnCase, async: true

  test "requires user authentication on all actions", %{conn: conn} do
    Enum.each([
      get(conn, ~p"/manage/videos"),
      get(conn, ~p"/manage/videos/new"),
      get(conn, ~p"/manage/videos/123"),
      get(conn, ~p"/manage/videos/123/edit"),
      put(conn, ~p"/manage/videos/123"),
      post(conn, ~p"/manage/videos"),
      delete(conn, ~p"/manage/videos/123")
    ], fn conn ->
      assert html_response(conn, 302)
      assert conn.halted
    end)
  end

  describe "with a logged-in user" do
    alias Rumbl.Multimedia

    @create_attrs %{url: "https://vide.os", title: "vid", description: "a vid"}
    @invalid_attrs %{title: "invalid"}

    setup %{conn: conn, login_as: username} do
      user = user_fixture(username: username)
      conn = assign(conn, :current_user, user)
      {:ok, conn: conn, user: user}
    end

    @tag login_as: "cat"
    test "lists all user's videos on index", %{conn: conn, user: user} do
      user_video = video_fixture(user, title: "funny cats")
      other_video = video_fixture(user_fixture(username: "other"), title: "another video")

      conn = get(conn, ~p"/manage/videos")
      response = html_response(conn, 200)
      assert response =~ ~r/Listing Videos/
      assert response =~ user_video.title
      refute response =~ other_video.title
    end

    @tag login_as: "cat"
    test "creates user video and redirects", %{conn: conn, user: user} do
      create_conn = post(conn, ~p"/manage/videos", video: @create_attrs)

      assert %{id: id} = redirected_params(create_conn)
      assert redirected_to(create_conn) == ~p"/manage/videos/#{id}"

      conn = get(conn, ~p"/manage/videos/#{id}")
      {video_id, _} = Integer.parse(id)
      assert html_response(conn, 200) =~ "Video #{video_id}"

      assert Multimedia.get_video!(id).user_id == user.id
    end

    @tag login_as: "cat"
    test "does not create vid, renders errors when invalid", %{conn: conn} do
      count_before = video_count()
      conn = post(conn, ~p"/manage/videos", video: @invalid_attrs)
      assert html_response(conn, 200) =~ "check the errors"
      assert video_count() == count_before
    end

    defp video_count, do: Enum.count(Multimedia.list_videos)
  end

  test "authorizes actions against access by other users", %{conn: conn} do
    owner = user_fixture(username: "owner")
    video = video_fixture(owner, @create_attrs)
    non_owner = user_fixture(username: "sneaky")
    conn = assign(conn, :current_user, non_owner)

    assert_error_sent :not_found, fn ->
      get(conn, ~p"/manage/videos/#{video}")
    end

    assert_error_sent :not_found, fn ->
      get(conn, ~p"/manage/videos/#{video}/edit")
    end

    assert_error_sent :not_found, fn ->
      put(conn, ~p"/manage/videos/#{video}", video: @create_attrs)
    end

    assert_error_sent :not_found, fn ->
      delete(conn, ~p"/manage/videos/#{video}")
    end
  end
end
