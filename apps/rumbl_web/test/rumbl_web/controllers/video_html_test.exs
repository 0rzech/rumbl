defmodule RumblWeb.VideoHTMLTest do
  use RumblWeb.ConnCase, async: true
  import Phoenix.Template

  test "renders index.html", %{conn: conn} do
    videos = [
      %Rumbl.Multimedia.Video{id: 1, title: "cats"},
      %Rumbl.Multimedia.Video{id: 2, title: "dogs"},
    ]

    content = render_to_string(RumblWeb.VideoHTML, "index", "html",
      conn: conn,
      videos: videos
    )

    assert String.contains?(content, "Listing Videos")

    for video <- videos do
      assert String.contains?(content, video.title)
    end
  end

  test "renders new.html", %{conn: conn} do
    changeset = Rumbl.Multimedia.change_video(%Rumbl.Multimedia.Video{})
    categories = [%Rumbl.Multimedia.Category{id: 123, name: "cats"}]

    content = render_to_string(RumblWeb.VideoHTML, "new", "html",
      conn: conn,
      changeset: changeset,
      categories: categories
    )

    assert String.contains?(content, "New Video")
  end
end
