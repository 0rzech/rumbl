defmodule RumblWeb.ChannelCase do
  @moduledoc """
  This module defines the test case to be used by
  channel tests.

  Such tests rely on `Phoenix.ChannelTest` and also
  import other functionality to make it easier
  to buyild common data structures and query the data layer.

  Finally, if the test case interacts with the database,
  it canot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # The default endpoint for testing
      @endpoint RumblWeb.Endpoint

      # Import conveniences for testing with channels
      import Phoenix.ChannelTest
      import RumblWeb.ChannelCase
    end
  end

  setup tags do
    Rumbl.DataCase.setup_sandbox(tags)
    :ok
  end
end
